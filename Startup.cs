using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using VolReadyDashboard.Data;

namespace VolReadyDashboard
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddSingleton<WeatherForecastService>();
            services.AddLogging();
            services.AddSingleton<ILogger>(provider =>
                provider.GetRequiredService<ILogger<SmartSheetService>>());
            
            services.AddSingleton<SmartSheetService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                var logger = endpoints.ServiceProvider.GetService<ILogger<Startup>>();
                var ssSvc = endpoints.ServiceProvider.GetService<SmartSheetService>();
                ssSvc.InitAsync().GetAwaiter().GetResult(); // yuk. Not perfect, but it works...

                endpoints.MapBlazorHub();
                endpoints.MapPost("/hooks/{name:alpha}", async context =>
                {
                    string name = context.Request.RouteValues["name"].ToString();
                    var challenge = context.Request.Headers["Smartsheet-Hook-Challenge"].ToString();
                    if (!string.IsNullOrEmpty(challenge))
                    {
                        await context.Response.WriteAsJsonAsync(new { smartsheetHookResponse = challenge });
                    }
                    else
                    {
                        var callbback = await context.Request.ReadFromJsonAsync<WebhookData>(context.RequestAborted);
                        // if no events, try callback instead

                        object responseObject = await ssSvc.WebHookCallbackAsync(name, callbback);
                        await context.Response.WriteAsJsonAsync(responseObject);
                    }
                });
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
