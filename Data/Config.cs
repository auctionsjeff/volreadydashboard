﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace VolReadyDashboard.Data
{
    public class Config
    {
        public string AccessToken;
        public string DashboardDataSheetId;
        public string DashboardConfigSheetId;
        public string ConfigSecret = "";
        public string EmailDataColumnName = "Email";
        public string TokenDataColumnName = "Token";

        public Config()
        {

        }

        public static string CONFIG_FNAME = "config.xml";

        public static Config GetConfig()
        {
            if (!File.Exists(CONFIG_FNAME)) // create config file with default values
            {
                using (FileStream fs = new FileStream(CONFIG_FNAME, FileMode.Create))
                {
                    XmlSerializer xs = new XmlSerializer(typeof(Config));
                    Config sxml = new Config();
                    xs.Serialize(fs, sxml);
                    return sxml;
                }
            }
            else // read configuration from file
            {
                using (FileStream fs = new FileStream(CONFIG_FNAME, FileMode.Open))
                {
                    XmlSerializer xs = new XmlSerializer(typeof(Config));
                    Config sc = (Config)xs.Deserialize(fs);
                    return sc;
                }
            }
        }

        public static bool SaveConfig(Config config)
        {
            if (!File.Exists(CONFIG_FNAME)) return false; // don't do anything if file doesn't exist

            using (FileStream fs = new FileStream(CONFIG_FNAME, FileMode.Open))
            {
                XmlSerializer xs = new XmlSerializer(typeof(Config));
                xs.Serialize(fs, config);
                return true;
            }
        }
    }
}
