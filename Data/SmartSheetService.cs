﻿using Microsoft.Extensions.Logging;
using Smartsheet.Net.Standard.Entities;
using Smartsheet.Net.Standard.Http;
using Smartsheet.Net.Standard.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VolReadyDashboard.Data
{
    public class SmartSheetService
    {
        private Config Config;
        private SmartsheetHttpClient SmartsheetHttpClient;
        private string UserName;
        private Sheet configSheet;
        private Sheet dataSheet;
        private ILogger _log;
        private const string HOOK_URL = "https://dashboard.volready.com/hooks";

        public bool ConfigValidated { get; private set; }

        public SmartSheetService(ILogger log)
        {
            // load from file on startup
            Config = Config.GetConfig();
            _log = log;
        }

        public async Task InitAsync()
        {
            _log.LogInformation("SmartSheetService InitAsync");

            // if config is valid, load the sheets
            var errs = ValidateConfigAsync().Result;
            if (errs.Count == 0)
            {
                dataSheet = await GetSheetByIdAsync(Config.DashboardDataSheetId);
                configSheet = await GetSheetByIdAsync(Config.DashboardConfigSheetId);

                // ensure webhooks for config and data
                var hooks = await SmartsheetHttpClient.GetWebhooksForUser(Config.AccessToken, true);
                var dataHook = hooks.Where(h => h.Name == "DashboardData").FirstOrDefault();
                var configHook = hooks.Where(h => h.Name == "DashboardConfig").FirstOrDefault();
                _log.LogInformation($"Found {hooks.Count()} existing webhooks.");

                if (dataHook == null)
                {
                    dataHook = new Webhook("DashboardData", $"{HOOK_URL}/DashboardData", "sheet", long.Parse(Config.DashboardDataSheetId), new string[] { "*.*" });
                    dataHook = await SmartsheetHttpClient.CreateWebhook(dataHook);
                    await EnableWebhookAsync(dataHook);
                }
                else if (dataHook.Enabled.Value == false)
                {
                    // log that it's shut off, but don't mess with it unless "not verified", in which case we need to "update" it
                    _log.LogError($"Data Webhook Disabled: {dataHook.Status}");
                    if (dataHook.Status == "NEW_NOT_VERIFIED")
                    {
                        await EnableWebhookAsync(dataHook);
                    }
                }

                if (configHook == null)
                {
                    configHook = new Webhook("DashboardConfig", $"{HOOK_URL}/DashboardConfig", "sheet", long.Parse(Config.DashboardConfigSheetId), new string[] { "*.*" });
                    configHook = await SmartsheetHttpClient.CreateWebhook(configHook);
                    await EnableWebhookAsync(configHook);
                }
                else if (configHook.Enabled.Value == false)
                {
                    // log that it's shut off, but don't mess with it unless "not verified", in which case we need to "update" it
                    _log.LogError($"Config Webhook Disabled: {configHook.Status}");
                    if (configHook.Status == "NEW_NOT_VERIFIED")
                    {
                        await EnableWebhookAsync(configHook);
                    }
                }
            }
        }

        private async Task<Webhook> EnableWebhookAsync(Webhook webhook)
        {
            // don't use "UpdateWebhook" since the platform complains. API docs show just setting "enabled": true
            var result = await SmartsheetHttpClient.ExecuteRequest<Webhook, string>(HttpVerb.PUT, $"webhooks/{webhook.Id}", "{ \"enabled\": true }");
            _log.LogInformation($"Created webhook: {webhook.Id}, {webhook.Name}");
            return result;
        }

        internal async Task<object> WebHookCallbackAsync(string name, WebhookData callback)
        {
            if (!string.IsNullOrEmpty(callback.NewWebHookStatus))
            {
                // only process webhook status
                _log.LogInformation($"Webhook event: {name} has new status {callback.NewWebHookStatus}");
                return new { ok = 200 };
            }
            if (callback.Events?.Count > 0)
            {
                // update sheets as necessary
                var firstEvent = callback.Events.FirstOrDefault();
                _log.LogInformation($"Got webhook {name}: {firstEvent.ObjectType}.{firstEvent.EventType}");
                // we can only debug in prod, so log as much as possible
                StringBuilder sb = new StringBuilder();
                foreach (var e in callback.Events)
                {
                    sb.AppendLine($"{e.ObjectType}.{e.EventType}, id {e.Id}, col {e.ColumnId}, row {e.RowId}");
                    //if (e.ObjectType == "row")
                    //{
                    //    // try to update just this row and merge into sheet                        
                    //} else if (e.ObjectType == "sheet" || e.ObjectType == "col")
                    //{
                    //    // other changes update whole sheet
                    //}
                }
                // update corresponding sheet
                if (name.Contains("Config"))
                {
                    configSheet = await GetSheetByIdAsync(Config.DashboardConfigSheetId);
                }
                else
                {
                    dataSheet = await GetSheetByIdAsync(Config.DashboardDataSheetId);
                }
            }
            return new { ok = 200 };
        }

        public Row GetCachedRowByEmail(string email)
        {
            return dataSheet.Rows.Where(r => r.GetValueForColumnAsString(Config.EmailDataColumnName).Equals(email)).FirstOrDefault();
        }

        public Row GetCachedRowByToken(string token)
        {
            return dataSheet.Rows.Where(r => r.GetValueForColumnAsString(Config.TokenDataColumnName).Equals(token)).FirstOrDefault();
        }

        public async Task<List<string>> ValidateConfigAsync()
        {
            List<string> errs = new List<string>();
            if (string.IsNullOrEmpty(Config.AccessToken))
            {
                errs.Add("Access Token missing");
            }

            if (string.IsNullOrEmpty(Config.DashboardConfigSheetId))
            {
                errs.Add("Dashboard Config Sheet ID missing");
            }

            if (string.IsNullOrEmpty(Config.DashboardDataSheetId))
            {
                errs.Add("Dashboard Data Sheet ID missing");
            }

            // those errors alone are fatal, so only continue if no errors yet
            if (errs.Count > 0)
            {
                return errs;
            }

            // now check that all configured sheets exist
            var dataTask = GetSheetNameByIdAsync(Config.DashboardDataSheetId);
            var configTask = GetSheetNameByIdAsync(Config.DashboardConfigSheetId);
            string dataSheetName = await dataTask.ConfigureAwait(false);
            string configSheetName = await configTask.ConfigureAwait(false);

            if (string.IsNullOrEmpty(dataSheetName))
            {
                errs.Add("Dashboard Data Sheet does not exist");
            }
            if (string.IsNullOrEmpty(configSheetName))
            {
                errs.Add("Dashboard Config Sheet does not exist");
            }

            // those errors are pretty bad too, so abort here if they happen
            if (errs.Count > 0)
            {
                return errs;
            }

            // now check smart sheet, access, etc
            if (!await CanWriteToSheetAsync(Config.DashboardDataSheetId).ConfigureAwait(false))
            {
                errs.Add($"User '{UserName}' is missing Write access to sheet '{dataSheetName}'. This is needed for Token generation. Please grant at least Editor privileges.");
            }

            if (errs.Count == 0)
            {
                ConfigValidated = true;
            }
            return errs;
        }

        private async void InitConenction()
        {
            if (SmartsheetHttpClient == null)
            {
                if (Config == null || string.IsNullOrEmpty(Config.AccessToken))
                {
                    throw new ArgumentNullException("Config.AccessToken");
                }
                SmartsheetHttpClient = new SmartsheetHttpClient(Config.AccessToken, null);

                // populate user name
                var user = await GetUserNameAsync().ConfigureAwait(false);
                UserName = user.FirstName + " " + user.LastName;
            }
        }

        private async Task<User> GetUserNameAsync()
        {
            InitConenction();
            return await SmartsheetHttpClient.GetCurrentUser().ConfigureAwait(false);
        }

        public async Task<Sheet> GetSheetByIdAsync(string sheetId, string accessToken = null)
        {
            if (accessToken == null) InitConenction();
            if (SmartsheetHttpClient == null) SmartsheetHttpClient = new SmartsheetHttpClient(accessToken, null);

            return await SmartsheetHttpClient.GetSheetById(long.Parse(sheetId)).ConfigureAwait(false);
        }

        private async Task<string> GetSheetNameByIdAsync(string id)
        {
            InitConenction();
            Sheet sheet = await GetSheetByIdAsync(id).ConfigureAwait(false);
            return sheet.Name;
        }

        private async Task<bool> CanWriteToSheetAsync(string sheetId)
        {
            InitConenction();
            Sheet sheet = await GetSheetByIdAsync(sheetId).ConfigureAwait(false);
            return sheet.AccessLevel != Smartsheet.Net.Standard.Definitions.AccessLevel.Viewer;
        }

        public string GetConfigSecret()
        {
            return Config.ConfigSecret;
        }

        public void SetConfig(Config config)
        {
            Config = config;
            Config.SaveConfig(config);
            InitConenction();
        }

        public async Task<List<Sheet>> SearchSheetsByNameAsync(string searchText, string accessToken)
        {
            // works without a valid config
            try
            {
                var results = await new SmartsheetHttpClient(accessToken, null).ListSheets(accessToken).ConfigureAwait(false);
                // TODO: use real search...
                // await SmartsheetHttpClient.ExecuteRequest<IndexResultResponse<Sheet>, Sheet>(HttpVerb.GET, "sheets?query={searchText}&scopes=sheetNames", null, accessToken: accessToken);
                return results.Where(s => s.Name.ToLower().Contains(searchText.ToLower())).ToList();
            }
            catch (Exception)
            {
                return new List<Sheet>();
            }
        }

        public async Task<bool> ValidateAccessTokenAsync(string token)
        {
            // works without a valid config
            try
            {
                var result = await new SmartsheetHttpClient(token, null).GetCurrentUser().ConfigureAwait(false);
                return result != null;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
