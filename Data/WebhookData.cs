﻿using Smartsheet.Net.Standard.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VolReadyDashboard.Data
{
    public class WebhookData
    {
        public string ChangeAgent { get; set; }
        public string Challenge { get; set; }
        public string Nonce { get; set; }
        public string Timestamp { get; set; }
        public long? WebhookId { get; set; }
        public string Scope { get; set; }
        public long? ScopeObjectId { get; set; }
        public string NewWebHookStatus { get; set; }
        public List<EventData> Events { get; set; }
    }

    public class EventData
    {
        public string ObjectType { get; set; }
        public string EventType { get; set; }
        public string ChangeAgent { get; set; }
        public long? Id { get; set; }
        public long? RowId { get; set; }
        public long? ColumnId { get; set; }
        public long? UserId { get; set; }
        public string Timestamp { get; set; }
    }
}
